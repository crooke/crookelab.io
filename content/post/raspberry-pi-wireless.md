---
title: "Raspberry Pi Wireless"
date: 2019-01-06T21:19:00-06:00
---

This weekend I battled configuring the wireless on a Raspberry Pi (specifically the Zero W) for hours, twice. Thought I'd share some findings.

There's good [documentation](https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md) on the RPi website, but it seems like there's also some missing info (or I didn't read it thoroughly enough...). 

If it helps anyone, here is the `/etc/wpa_supplicant/wpa_supplicant.conf` file that ultimately worked for me: 

```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=US

network={
        ssid="MyWifi"
        psk="MyWifiPa$$word"
        key_mgmt=WPA-PSK
}
```

It looks like the the first two lines are required by wpa_cli which I believe is what the Pi uses in the `raspi-config` tool to setup wireless. ArchLinux wiki has a good [page](https://wiki.archlinux.org/index.php/WPA_supplicant#Connecting_with_wpa_cli) explaining some of the wpa_supplicant.conf file and configuring wireless on Linux.

Also, don't forget you can add the `wpa_supplicant.conf` file to the `boot` partition of the SD card before plugging in your Pi and Raspbian OS will copy it to /etc/wpa_supplicant/ and set it all up for you (hopefully).
