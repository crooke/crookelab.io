---
title: "Raspberry Pi I2C (with the MAX30101)"
date: 2019-06-12T22:11:04-05:00
---

I wanted to write some C code on the Raspberry Pi to interact with a [MAX30101 sensor](https://www.maximintegrated.com/en/products/sensors/MAX30101.html). This proved more challenging than expected mostly because I couldn't find good documentation for I<sup>2</sup>C -- the protocol used to communicate with the MAX sensor -- on the Pi/Linux. I did finally come across a great reference on none other than the [Linux git repo](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/i2c/dev-interface).

Here's an example program I came up with for reading a couple registers from the MAX30101:

<script src="https://gist.github.com/crooke/2e7d0114ecbc1149d0bf77b9e4070c77.js"></script>

As mentioned in the Linux reference, you may need to install libi2c-dev: `sudo apt install libi2c-dev`. Then you should be able to compile and run it on the Pi with `gcc raspberry-pi-i2c-max30101.c -o raspberry-pi-i2c-max30101 && ./raspberry-pi-i2c-max30101`.

I should also give much credit to [David Greyson's blog post](http://blog.davidegrayson.com/2012/06/using-ic-on-raspberry-pi.html).
