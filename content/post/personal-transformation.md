---
title: "Personal Transformation"
date: 2018-09-22T12:58:37-05:00
draft: true
---

I'm reading an essay by Dallas Willard titled [*Living A Transformed Life Adequate To Our Calling*](/docs/living-a-transformed-life-adequate-to-our-calling.pdf).
It's been interesting and I think quite timely for me. I've been thinking about my life and I guess
the trajectory of my life lately. Also wrestling with my desires. There's another topic in itself - 
desires. But let me just lay out some of his main points thus far.

There is a *general pattern* to personal transformation. As a software guy, this already intrigues
me because of the importance of patterns in software design. But anyways, the patterns that make for
effective transformation are:

1. Vision - why this transformation is valuable or desirable
2. Intention - decide or will to bring vision to reality
3. Means - the instruments or tools by which you achieve the transformation

To help remember the patterns, he turns it into an acronym - VIM. Vim! The timeless, powerful,
fast, beautiful-in-its-simplicity text editor! Now I'm hooked on what he has to say. (What a nerd..)

He uses examples of learning a new language and going through Alcoholics Anonymous, but the point of the article is spiritual formation, specifically being transformed into Christlikeness. So he applies VIM to this sort of transformation.

## Vision - Life in the Kingdom

Life in the Kingdom of God is the vision we must have in order to attain Christlikeness. I wonder if here I should stop and consider why Christlikeness is the goal in the first place.. Willard defines the kingdom of God as *the range of God's effective will*. It is interesting that God allows his will to not always happen on earth. Like Jesus' prayer: your kingdom come, your will be done - on earth as it is in heaven.

## Intention - Obedience

To intend to live in the kingdom of God is to obey Jesus as Lord. To believe or trust what he says. To believe is to act, not just *know*. This is the problem many of us as professing Christians have with the idea of "faith"- that it can be divorced of action. Willard says: "If you don't intend to follow someone's advice, you simply don't trust them." Where do I stand in this regard?

## Thoughts/Questions

Why is Christlikeness the goal? Not saying it's bad, maybe more just looking for more scriptural support for it. Is it the end-all-be-all of what it means to be a Christian? I think so... Meaning of life in general?