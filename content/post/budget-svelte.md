---
title: "Budget App and Svelte"
date: 2020-04-04T19:20:28-05:00
---

This is about two things - a budget app I built and the tool I used to do it.

I wanted to build a budget app because even though a spreadsheet is usually good enough for me, my wife has a hard time following it.
Which is no fault of hers - I just can't help myself and end up making it way too complicated.
But I also think a lot of budgeting tools and resources out there are either too complicated or too vague.
I wanted to build something simple that helps you understand how much you make and how much you can spend.
Plus, it's an excuse to code and build something.

I could've used React, since I have experience with it.
But I don't know that I'm too fond of React for various reasons, and I've been wanting to try [Svelte](https://svelte.dev) out for a while now.
Svelte has been really enjoyable to work with.
It feels closer to working with raw HTML, CSS, and JS than a framework like React, while still giving you the power, reusability, and abstractions.
Overall, I'd say Svelte feels more intuitive.

So here it is, the cleverly named [Budget App](https://crooke.gitlab.io/budget-app/).

[Source code](https://gitlab.com/crooke/budget-app)
