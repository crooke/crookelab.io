---
title: "About"
date: 2020-04-04T19:19:05-05:00
draft: false
---

I'm a guy living in Lawrence, KS.

I enjoy writing code, which is what a lot of this site is about, along with some other musings maybe.

## Projects

#### [Budget App](https://crooke.gitlab.io/budget-app)

Simple tool to help you setup a budget. No login required. Saves data in local browser storage.

[Blog post]({{< ref "/post/budget-svelte.md" >}})

[Source code](https://gitlab.com/crooke/budget-app)

#### [Cook Easy](https://master.d38o24qnykwkx0.amplifyapp.com/meal-plan)

Meal planning tool. It's primarily a proof of concept. Closed source for now.
